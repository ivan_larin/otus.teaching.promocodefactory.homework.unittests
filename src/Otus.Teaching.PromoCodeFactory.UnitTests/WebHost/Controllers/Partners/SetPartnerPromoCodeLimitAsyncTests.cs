﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerNotFound_ShouldReturnNotFound()
        {
            //Arrange
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner) null);
            var testee = new PartnersController(partnerRepositoryMock.Object);
            
            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerIsNotActive_ShouldReturnBadRequest()
        {
            //Arrange
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            var partner = new Partner()
            {
                IsActive = false
            };
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            var testee = new PartnersController(partnerRepositoryMock.Object);

            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.StatusCode.Should().Be(400);
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Данный партнер не активен");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfHasActiveLimit_ShouldSetPartnerNumberIssuedPromoCodesToZero()
        {
            //Arrange
            var activeLimit = new PartnerPromoCodeLimit();
            var partner = new Partner()
            {
                IsActive = true,
                NumberIssuedPromoCodes = 123,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    activeLimit
                }
            };
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            var testee = new PartnersController(partnerRepositoryMock.Object);

            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            activeLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfHasNotActiveLimit_ShouldNotSetPartnerNumberIssuedPromoCodesToZero()
        {
            //Arrange
            var canceledLimit = new PartnerPromoCodeLimit()
            {
                CancelDate = DateTime.Now
            };
            var partner = new Partner()
            {
                IsActive = true,
                NumberIssuedPromoCodes = 123,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    canceledLimit
                }
            };
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            var testee = new PartnersController(partnerRepositoryMock.Object);

            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(123);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitIs0_ShouldReturnBadRequest()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            var testee = new PartnersController(partnerRepositoryMock.Object);
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 0
            };
            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsSavedToDatabase()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            var testee = new PartnersController(partnerRepositoryMock.Object);
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 1
            };
            //Act
            var result = await testee.SetPartnerPromoCodeLimitAsync(Guid.Empty, setPartnerPromoCodeLimitRequest);

            //Assert
            partner.PartnerLimits.Count.Should().Be(1);
            partnerRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }
    }
}